<?php

namespace App\Http\Controllers;
use App\Models\PostLike;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        if(Auth::user()) 
        {
            $post = new Post();
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);

            $post->save();

            return redirect('/posts');
        } else
        {
            return redirect('/login');
        }
    }

    public function index() 
    {
        $posts = Post::where('is_active', true)->get();
        return view('posts.index', ['posts' => $posts]);
    }

    public function welcome() 
    {
        $posts = Post::all();
        $posts = $posts->shuffle()->take(3);
        return view('welcome', ['posts' => $posts]);
    }
    

    public function myPosts(Request $request) 
    {
        if (Auth::user()) 
        {
            // $posts = Auth::user()->posts;
            // return view('posts.index')->with('posts', $posts);
            $posts = Post::where('user_id', Auth::user()->id)->get();
            return view('posts.index', ['posts' => $posts]);
        } else 
        {
            return redirect('/login');
        }
    }

    public function show($id) 
    {
        $post = Post::find($id);
        return view('posts.show', ['post' => $post]);
    }

    public function edit($id) 
    {
        $post = Post::find($id);
        return view('posts.edit', ['post' => $post]);
    }

    public function update(Request $request, $id) 
    {
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id) {
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        } 
        return redirect('/posts/' . $id);
    }

    
    public function destroy($id) 
    {
        $post = Post::find($id);
        if(Auth::user()->id === $post->user_id) {
            $post->delete();
        }
        return redirect('/posts');
    }

    
    public function archive($id) 
    {
        $post = Post::find($id);
        if(Auth::user()->id === $post->user_id) {
            $post->is_active = false;
            $post->save();
        }
        return redirect('/myPosts');
    }

   
    public function unarchive($id) 
    {
        $post = Post::find($id);
        if(Auth::user()->id === $post->user_id) {
            $post->is_active = true;
            $post->save();
        }
        return redirect('/myPosts');
    }

    public function like($id) 
    {
        
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        if($user_id !== $post->user_id) {
             
            //  if ($post->likes->contains('user_id', $user_id)) {
            //     PostLike::where('post_id', $id)->where('user_id', $user_id)->delete();
            //  } else {
            //     $postLike = new PostLike();
            //     $postLike->post_id = $id;
            //     $postLike->user_id = $user_id;
            //     $postLike->save();
            //  }
             
            
            if ($post->likes->where('user_id', $user_id)->count() === 0) {
                $post->likes()->create([
                    'user_id' => $user_id,
                ]);
            } else {
                $post->likes->where('user_id', $user_id)->first()->delete();
            }
        }
        return redirect('/posts/' . $id);
    }

    public function comment(Request $request, $id) 
    {
        $post = Post::find($id);
        $user_id = Auth::user()->id;

        $post->comments()->create([
            'user_id' => $user_id,
            'content' => $request->input('content'),
            'datetime_commented' => date('Y-m-d H:i:s'),
        ]);
        return redirect('/posts/' . $id);
    }


}
