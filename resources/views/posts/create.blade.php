@extends('layouts.app')

@section('content')

    <form method="POST" action="/posts">
        @csrf
        <h1>Create a Post</h1>
        <div class="form-group">
            <label for="title" >Title:</label>
            <input type="text" id="title" name="title" class="form-control" id="title">
        </div>
        <div class="form-group">
            <label for="content" >Content:</label>
            <textarea class="form-control" id="content" name="content" rows="3"></textarea>
        </div>
        <div class="mt-2">
            <button type="submit" class="btn btn-primary">Create Post</button>
        </div>
    </form>

@endsection