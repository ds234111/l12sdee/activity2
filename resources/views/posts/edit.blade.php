@extends('layouts.app')

@section('content')
    @if($post->exists && $post->user_id == Auth::id())
        <form method="POST" action="/posts/{{ $post->id }}">
            @method('PUT')
            @csrf
            <h1>{{ $post->exists ? 'Edit Post' : 'Create Post' }}</h1>
            <div class="form-group">
                <label for="title" >Title:</label>
                <input type="text" id="title" name="title" class="form-control" id="title" value="{{ $post->title }}">
            </div>
            <div class="form-group">
                <label for="content" >Content:</label>
                <textarea class="form-control" id="content" name="content" rows="3">{{ $post->content }}</textarea>
            </div>
            <div>
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="/posts">Cancel</a>
            </div>
    @else
        <form method="POST" action="/posts">
        <p>Sorry, you can't edit this post.</p>
    @endif     
    </form>
@endsection