@extends('layouts.app')

@section('content')
      
    @if(count($posts) > 0 )
        @foreach($posts as $post)
            <div class="card text-center mb-3">

                {{-- Card Body --}}
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="/posts/{{$post->id}}" >{{$post->title}}</a>
                    </h4>
                    <h6 class="card-text mb-3">
                        Author: {{$post->user->name}}
                    </h6>
                    {{-- isactive --}}
                    @if($post->is_active == 1)
                        <p class="card-text mb-3">Status: Active</p>
                    @endif
                    
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                </div>

                {{-- Check if auth user --}}
                @if(Auth::user())
                    @if(Auth::user()->id == $post->user_id)
                        <div class="mb-3">
                            <form method="POST" action="/posts/{{$post->id}}">
                                @method('DELETE')
                                @csrf
                                
                                {{-- unarchive --}}
                                @if ($post->is_active == 0)
                                    <a href="/posts/{{$post->id}}/unarchive" class="btn btn-success">Unarchive Post</a>
                                @else
                                    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit Post</a>
                                    <button type="submit" class="btn btn-danger">Delete Post</button>
                                @endif
                            </form>
                        </div>
                    @endif
                @endif


            </div>
        @endforeach
        
    @else 
        <div>
            <h2>There are no post to show</h2>
            <a href="/posts/create" class="btn btn-info">Create Post</a>
        </div>
    @endif
@endsection
