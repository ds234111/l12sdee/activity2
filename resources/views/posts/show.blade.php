@extends('layouts.app')

@section('content')
    <div class="card"> 
        <div class="card-body">
            <h2>{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>
            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains("user_id", Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif
            {{-- create comment with text area and submit button --}}
            <div class="mt-3">
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
                    @method('POST')
                    @csrf
                    {{-- extents text area to form width --}}
                    <div class="form-group">
                        
                        <textarea name="content" id="content" cols="60" rows="auto"></textarea>
                    </div>
                    
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Post Comment</button>
                    </div>
                </form>
            </div>   
            
            <div class="mt-3">
                <a href="/myPosts" class="card-link">View all posts</a>
            </div> 
        </div>
        
    </div>

    <form class="d-inline" method="POST" action="/posts/{{$post->id}}/comment">
        <h3 class="card-text">Comments:</h3>
        @foreach($post->comments as $comment)
            <div class="card">
                <div class="card-body">
                    <p class="card-text">{{$comment->content}}</p>
                    <p class="card-subtitle text-muted">Author: {{$comment->user->name}}</p>
                    <p class="card-subtitle text-muted">Created at: {{$comment->datetime_commented}}</p>
                </div>
            </div>
        @endforeach
@endsection
