@extends('layouts.app')

@section('content') 
    {{-- image path:public/img/banner.jpg --}}  
    <div class="jumbotron text-center">
        <img src="/img/banner.jpg" alt="banner" class="img-fluid mb-3">
        <h2>Featured Posts:</h2>
    </div>
    
    @if(count($posts) > 0 )
        @foreach($posts as $post)
            <div class="card text-center mb-3">

                {{-- Card Body --}}
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="/posts/{{$post->id}}" >{{$post->title}}</a>
                    </h4>
                    <h6 class="card-text mb-3">
                        Author: {{$post->user->name}}
                    </h6>
                </div>
            </div>
        @endforeach
        
    @else 
        <div>
            <h2>There are no post to show</h2>
            <a href="/posts/create" class="btn btn-info">Create Post</a>
        </div>
    @endif
@endsection
