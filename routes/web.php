<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::post('/posts', [PostController::class, 'store']);
Route::get('/posts', [PostController::class, 'index']);
Route::get('/myPosts', [PostController::class, 'myPosts']);
Route::get('/posts/create', [PostController::class, 'create']);
Route::get('/', [PostController::class, 'welcome']);
Route::get('/posts/{id}', [PostController::class, 'show']);
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);
Route::get('/posts/{id}/unarchive', [PostController::class, 'unarchive']);
Route::put('/posts/{id}', [PostController::class, 'update']);
// Route::delete('/posts/{id}', [PostController::class, 'destroy']);
Route::delete('/posts/{id}', [PostController::class, 'archive']);
Route::put('/posts/{id}/like', [PostController::class, 'like']);
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);
